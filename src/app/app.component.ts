import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Http and Routings';

  public totalItem : number = 1;
  public grandTotal !: number;

  constructor(private cartService : CartService, ) { }

  ngOnInit(): void {
    this.cartService.getCartList()
    .subscribe(res=>{
      this.totalItem = res.length;
    })
  }
}
