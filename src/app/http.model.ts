export interface Employee {
    id: number,
    price: number,
    title: string
}