import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartList = new BehaviorSubject<any>([])
  cartItem: any[] = [];

  constructor() { }
  getCartList() {
    return this.cartList.asObservable();
  }

  pushData(item: any) {
    this.cartItem.push(item)
    this.cartList.next(this.cartItem);
  }

  addtoCart(item: any) {
    this.cartItem.push(item);
    this.cartList.next(this.cartItem);
    this.getTotalPrice();
  }

  getTotalPrice() : number{
    let grandTotal = 0;
    this.cartItem.map((a:any)=>{
      grandTotal += a.total;
    })
    return grandTotal;
  }

  // getTotalPrice(): number {
  //   let grandTotal = 0;
  //   this.cartItem.forEach((item: any) => {
  //     if (item.total && typeof item.total === 'number') {
  //       grandTotal += item.total || 0;
  //     }
  //   });
  //   return grandTotal;
  // }

  removeCartItem(product: any){
    this.cartItem.map((a:any, index:any)=>{
      if(product.id=== a.id){
        this.cartItem.splice(index,1);
      }
    })
    this.cartList.next(this.cartItem);  
    this.getTotalPrice();
  }
  
  removeAllCart(){
    this.cartItem = []
    this.cartList.next(this.cartItem);
    this.getTotalPrice();
  }

}
