import { Component } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})
export class LinkComponent {
  title = 'Http and Routings';

  public totalItem : number = 1;
  public grandTotal !: number;

  constructor(private cartService : CartService, ) { }

  ngOnInit(): void {
    this.cartService.getCartList()
    .subscribe(res=>{
      this.totalItem = res.length;
    })
  }
}
