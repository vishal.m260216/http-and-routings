import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private _url : string = "https://fakestoreapi.com/products";

  constructor(private http:HttpClient) { }

  getApi(){
    return this.http.get(this._url)
  }

  getProduct(id:number){
    return this.http.get(`https://fakestoreapi.com/products/${id}`)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}
