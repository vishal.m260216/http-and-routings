import { Component, Injectable, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';

@Injectable()
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  items:any[] = [];
  public grandTotal !: number;

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cartService.getCartList().subscribe((res) => {
      console.log(res);
      this.items = res;
      this.grandTotal = this.cartService.getTotalPrice();
    })
  }

  removeItem(item: any){
    this.cartService.removeCartItem(item);
  }
  emptycart(){
    this.cartService.removeAllCart();
  }
}
