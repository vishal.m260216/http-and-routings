import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart.service';
import { HttpService } from '../../http.service';
import { ActivatedRoute,} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductComponent implements OnInit {
  public Apis: any = [];
  isProductPage = false;

  constructor(private cartService: CartService, private route: ActivatedRoute, private apiService: HttpService) { }

  ngOnInit() {
    this.apiService.getApi().subscribe((res) => {
      this.Apis = res;
      this.Apis.forEach((a:any) =>{
        if(a.category ==="women's clothing" || a.category ==="men's clothing"){
          a.category = "fashion"
        }
        Object.assign(a,{quantity:1,total:a.price})
      })
      console.log(this.Apis)
    })
    this.route.queryParams.subscribe((res: any) => {
      if (res.page === 'product') {
        this.isProductPage = true;
      } else {
        this.isProductPage = false;
      }
    })
  }

  addtocart(item: any){
    this.cartService.addtoCart(item);
  }
}
