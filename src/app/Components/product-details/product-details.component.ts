import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CartService } from 'src/app/cart.service';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit {
productDetails:any = '';

  constructor(private cartService: CartService, private router: Router, private route: ActivatedRoute, private apiService: HttpService) { }

  ngOnInit() {
    this.route.params.subscribe((res:any)=>{
      this.apiService.getProduct(res.id).subscribe((product:any)=>{
        this.productDetails = product;
        console.log(this.productDetails);
      })
      
    })
  }

  onClick () {
      this.cartService.pushData(this.productDetails);
      this.router.navigate(['cart'])
  }

  click () {
    this.router.navigate(['cart'])
}
}
