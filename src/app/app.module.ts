import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './http.service';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { AboutComponent } from './Components/about/about.component';

import { CartComponent } from './Components/cart/cart.component';
import { MatIconModule } from '@angular/material/icon';
import { ProductDetailsComponent } from './Components/product-details/product-details.component';
import { ProductComponent } from './Components/products/products.component';
import { FilterPipe } from './shared/filter.pipe';
import { CartService } from './cart.service';
import { LinkComponent } from './link/link.component';

@NgModule({
  declarations: [
    AppComponent,
    LinkComponent,
    HomeComponent,
    AboutComponent,
    CartComponent,
    ProductDetailsComponent,
    ProductComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    MatIconModule,
  ],
  providers: [HttpService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
